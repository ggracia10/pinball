﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ramp : MonoBehaviour {

    public int speed;
    public Vector3 direction;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay(Collider collision)
    {
        if(collision.gameObject.tag == "Ball")
        {
            Debug.Log("HEY");
            collision.GetComponent<Rigidbody>().AddForce(direction * speed, ForceMode.Force);
        }
    }
}
