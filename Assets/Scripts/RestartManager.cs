﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartManager : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Ball")
        {
            GameObject.Find("PinballManager").GetComponent<PinballManager>().RestartGame();
        }
    }
}
