﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper : MonoBehaviour {

    [SerializeField] float bounciness;
    [SerializeField] Light emission;
    PinballManager pm;

    private void Start()
    {
        pm = GameObject.Find("PinballManager").GetComponent<PinballManager>();
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            
            pm.SetScoreUp(30);
            foreach(var contact in collision.contacts)
            {
                contact.otherCollider.GetComponent<Rigidbody>().AddForce(-1 * contact.normal * bounciness, ForceMode.Impulse);
            }
            StartCoroutine(LightOn());
            pm.PlayBumperAudio();

        }
    }

    IEnumerator LightOn()
    {
        for(float i = 0; i < 1; i+=Time.deltaTime*10)
        {
            emission.intensity = i;
            yield return null;
        }
        for (float i = 1; i > 0; i -= Time.deltaTime * 10)
        {
            emission.intensity = i;
            yield return null;
        }
    }
}
