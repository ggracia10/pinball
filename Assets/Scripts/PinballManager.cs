﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PinballManager : MonoBehaviour
{
    enum Flippers { RIGHT, LEFT };

    JointSpring []spring;
    [SerializeField] HingeJoint []hingeJoint;
    [SerializeField] float []restPosition;
    [SerializeField] float []pressedPosition;
    [SerializeField] float []flipperStrength;
    [SerializeField] float []flipperDamper;
    [SerializeField] float []direction;
    [SerializeField] Rigidbody rbBall;

    [SerializeField] Rigidbody plungerRB;
    Vector3 plungerIniPos;
    [SerializeField] float plungerForce;
    bool plungerActive;
    [SerializeField] float plungerMaxOffset;
    float plungerOffset;
    float timePlunger;
    [SerializeField] float timeMaxPlunger;
    bool movePlunger;

    Vector3 ballIniPos;
    bool ballActive;
    int currentCredits;

    [SerializeField] Target[] target;

    [SerializeField] Text scoreTxt;
    [SerializeField] Text creditsTxt;
    [SerializeField] Text multText;
    [SerializeField] Text insertTxt;
    [SerializeField] Text highscoreTxt;
    int score;
    int scoreMult;

    [SerializeField] AudioSource bumperSnd;
    [SerializeField] AudioSource flipperSnd;
    [SerializeField] AudioSource coinSnd;
    [SerializeField] AudioSource plungerSnd;
    [SerializeField] AudioSource targetSnd;

    int highScore;
    bool showText;
    float timeText;

    // Use this for initialization
    void Start()
    {
        score = 0;
        highScore = 4590;
        scoreMult = 1;
        multText.text = "x1.0";
        highscoreTxt.text = "Hihgscore: " + itos(highScore);
        scoreTxt.text = "Score: " + itos(score);
        ballIniPos = rbBall.transform.position;
        ballActive = false;
        rbBall.gameObject.SetActive(false);
        currentCredits = 0;
        creditsTxt.text = "Credits: " + currentCredits;
        spring = new JointSpring[2];
        for (int i = 0; i < 2; i++)
        {
            spring[i] = new JointSpring();
            spring[i].spring = flipperStrength[i];
            spring[i].damper = flipperDamper[i];
            spring[i].targetPosition = restPosition[i];
            hingeJoint[i].spring = spring[i];
            hingeJoint[i].useSpring = true;
        }
        rbBall.maxAngularVelocity = 250;
        plungerIniPos = plungerRB.transform.localPosition;
        movePlunger = false;
    }

    private void FixedUpdate()
    {
        if(movePlunger)
        {
            plungerRB.AddForce(Vector3.back * plungerForce * 100000 * (timePlunger/timeMaxPlunger), ForceMode.Impulse);
            movePlunger = false;
        }
    }

    void Update()
    {
        if(currentCredits == 0)
        {
            ShowTextInsert();
        }
        else
        {
            insertTxt.enabled = false;
        }
        if(Input.GetKeyDown(KeyCode.Return))
        {
            if (currentCredits == 0)
            {
                StartCoroutine(SetTextScore(0, 0));
                scoreMult = 1;
                multText.text = "x1.0";
                for (int i = 0; i < target.Length; i++)
                {
                    if(!target[i].GetTargetUp())
                        target[i].WaitToSetTargetUp();
                }
                currentCredits = 3;
                if (!ballActive)
                {
                    currentCredits++;
                    ballActive = true;
                    rbBall.gameObject.SetActive(true);
                    RestartGame();
                    coinSnd.Play();
                }
                creditsTxt.text = "Credits: " + (currentCredits);
            }
        }
        FlippersInput();
        PlungerInput();

        if (!movePlunger && timePlunger == 0)
            plungerRB.constraints = RigidbodyConstraints.FreezeAll;
        else
            plungerRB.constraints = RigidbodyConstraints.FreezeRotation;
    }

    void ShowTextInsert()
    {
        timeText += Time.deltaTime;
        if (timeText > 1)
        {
            timeText = 0;
            showText = !showText;
            insertTxt.enabled = showText;
        }
    }

    public void CheckTargetDown()
    {
        int targetDown = 0;
        for(int i = 0; i < target.Length; i++)
        {
            if (!target[i].GetTargetUp())
                targetDown++;
        }
        if(targetDown == target.Length)
        {
            scoreMult *= 2;
            multText.text = "x" + scoreMult + ".0";
            for(int i = 0; i < target.Length; i++)
            {
                target[i].WaitToSetTargetUp();
            }
        }
    }

    void PlungerInput()
    {
        if (Input.GetKey(KeyCode.Space)) {
            if(timePlunger < timeMaxPlunger)
                timePlunger += Time.deltaTime;
            plungerOffset = plungerMaxOffset * (timePlunger / timeMaxPlunger);
            plungerRB.transform.localPosition = plungerIniPos + Vector3.forward * plungerOffset;
            plungerRB.constraints = RigidbodyConstraints.FreezeRotation;
        }

        if(Input.GetKeyUp(KeyCode.Space))
        {
            movePlunger = true;
            plungerSnd.Play();
        }
        
        if(plungerRB.transform.localPosition.z < plungerIniPos.z)
        {
            plungerOffset = 0;
            timePlunger = 0;

            plungerRB.velocity = Vector3.zero;

            plungerRB.transform.localPosition = plungerIniPos + Vector3.forward * plungerOffset;
        }
        
    }

    void FlippersInput()
    {
        if(Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow))
            flipperSnd.Play();
        if (Input.GetKey(KeyCode.RightArrow))
        {
            spring[(int)Flippers.RIGHT].targetPosition = pressedPosition[(int)Flippers.RIGHT];
            hingeJoint[(int)Flippers.RIGHT].spring = spring[(int)Flippers.RIGHT];
        }
        else
        {
            spring[(int)Flippers.RIGHT].targetPosition = restPosition[(int)Flippers.RIGHT];
            hingeJoint[(int)Flippers.RIGHT].spring = spring[(int)Flippers.RIGHT];
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            spring[(int)Flippers.LEFT].targetPosition = pressedPosition[(int)Flippers.LEFT];
            hingeJoint[(int)Flippers.LEFT].spring = spring[(int)Flippers.LEFT];
        }
        else
        {
            spring[(int)Flippers.LEFT].targetPosition = restPosition[(int)Flippers.LEFT];
            hingeJoint[(int)Flippers.LEFT].spring = spring[(int)Flippers.LEFT];
        }
    }

    string itos (int _score)
    {
        int numb = 0;
        int[] arrayNumb = new int [5];
        string ret = "";
        for(int i = 0; i < 5; i++)
        {
            arrayNumb[4 - i] = _score % 10;
            _score /= 10;
            numb = i+1;
            
            if (_score < 1)
                break;
        }
        for(int i = 0; i < 5-numb; i++)
        {
            ret += '0';
        }
        for(int i = 5-numb; i < 5; i++)
        {
            ret += arrayNumb[i].ToString();
        }
        return ret;
    }

    public void SetScoreUp(int i)
    {
        StartCoroutine(SetTextScore(score, score+i*scoreMult));
       
        score += i * scoreMult;
        if (score > highScore)
        {
            highScore = score;
            highscoreTxt.text = "Hihgscore: " + itos(highScore);
        }

        return;
    }

    public void RestartGame()
    {
        currentCredits--;
        if (currentCredits > 0)
        {
            rbBall.transform.position = ballIniPos;
        }
        else
        {
            StopAllCoroutines();
            scoreTxt.text = "Score: " + itos(score);
            EndGame();
            ballActive = false;
            rbBall.gameObject.SetActive(false);
        }
        creditsTxt.text = "Credits: " + (currentCredits);
    }

    public void EndGame()
    {
        
        highScore = score;
        score = 0;
        
    }

    public void PlayBumperAudio()
    {
        bumperSnd.Play();
    }

    public void PlayTargetAudio()
    {
        targetSnd.Play();
    }

    IEnumerator SetTextScore(int lastScore, int finalScore)
    {
        yield return null;
        for(int i = lastScore; i <= finalScore; i++)
        {
            scoreTxt.text = "Score: " + itos(i);
            yield return null; ;
        }
    }
}