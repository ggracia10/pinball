﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

    bool isUp;
    bool moving;
    [SerializeField] Vector3 iniPos;
    [SerializeField] Vector3 offset;
    float curTime;
    [SerializeField] float timeMax;
    bool waiting;
    Light targetLight;

    [SerializeField] Color upColor;
    [SerializeField] Color downColor;

    // Use this for initialization
    void Start () {
        moving = false;
        isUp = true;
        waiting = false;
        targetLight = gameObject.GetComponentInParent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
		if(moving)
        {
            curTime += Time.deltaTime;
            if(curTime > timeMax)
            {
                moving = false;
                transform.position = iniPos + offset;
                return;
            }
            transform.position = iniPos + (curTime / timeMax) * offset;
        }

        else if(waiting)
        {
            curTime += Time.deltaTime;
            if(curTime > timeMax)
            {
                waiting = false;
                SetTargetUp(true);
            }
        }
	}

    public bool GetTargetUp()
    {
        return isUp;
    }

    public void WaitToSetTargetUp()
    {
        curTime = 0;
        waiting = true;
    }

    public void SetTargetUp(bool _isUp)
    {
        iniPos = transform.position;
        isUp = _isUp;
        offset *= -1;
        moving = true;
        curTime = 0;
        if (_isUp) targetLight.color = upColor;
        else targetLight.color = downColor;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(!moving)
        {
            GameObject.Find("PinballManager").GetComponent<PinballManager>().SetScoreUp(50);
            GameObject.Find("PinballManager").GetComponent<PinballManager>().PlayTargetAudio();
            SetTargetUp(false);
            GameObject.Find("PinballManager").GetComponent<PinballManager>().CheckTargetDown();
        }
    }
}
