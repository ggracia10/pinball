﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour {
    SpriteRenderer renderer;
    float red, green, blue, alfa;
    bool alfaUp;
    bool changeColor;
	// Use this for initialization
	void Start () {
        renderer = GetComponent<SpriteRenderer>();
        changeColor = true;
	}
	
	// Update is called once per frame
	void Update () {
        changeColor = false;
        if(alfaUp)
        {
            alfa += Time.deltaTime;
            if(alfa >= 1)
            {
                alfa = 1;
                alfaUp = false;
                changeColor = true;
            }
        }
        else if(!alfaUp)
        {
            alfa -= Time.deltaTime;
            if(alfa <= 0.57f)
            {
                alfa = 0.57f;
                alfaUp = true;
                changeColor = true;
            }
        }
        if (changeColor)
        {
            red = Random.Range(0.7f, 1f);
            green = Random.Range(0.7f, 1f);
            blue = Random.Range(0.7f, 1f);
        }
        renderer.color = new Color(red, green, blue, alfa);

    }
}
